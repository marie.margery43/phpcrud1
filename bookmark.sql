-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 09 fév. 2021 à 10:12
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bookmark`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `nom`, `description`) VALUES
(1, 'cuisine', 'Description favori cuisine'),
(2, 'infos', 'Description favori infos'),
(3, 'jeux', 'Description favori jeux'),
(4, 'sport', 'Description favori sport'),
(5, 'musique', 'Description favori musique'),
(6, 'livre', 'Description favori livre'),
(7, 'film', 'Description favori film'),
(8, 'people', 'Description favori people'),
(9, 'boulot', 'Description favori boulot'),
(10, 'enfants', 'Description favori enfants'),
(11, 'achat', 'Description favori achat');

-- --------------------------------------------------------

--
-- Structure de la table `ressource`
--

DROP TABLE IF EXISTS `ressource`;
CREATE TABLE IF NOT EXISTS `ressource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `lien` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ressource`
--

INSERT INTO `ressource` (`id`, `nom`, `lien`, `date`, `description`) VALUES
(1, 'site-essai', 'www.site-essai.com', '2021-02-05', 'description site essai1'),
(2, 'site-infos', 'www.site-infos.com', '2020-02-07', 'Description site infos'),
(3, 'site-jeux', 'www.site-jeux.com', '2020-03-12', 'Description site jeux'),
(4, 'site-sport', 'www.site-sport.com', '2020-04-12', 'Description site sport'),
(5, 'site-musique', 'www.site-musique.com', '2020-05-11', 'Description site musique'),
(6, 'site-livre', 'www.site-livre.com', '2020-06-19', 'Description site livre'),
(7, 'site-film', 'www.site-film.com', '2020-07-02', 'Description site film'),
(8, 'site-people', 'www.site-people.com', '2020-07-13', 'Description site people'),
(9, 'site-boulot', 'www.site-boulot.com', '2020-08-22', 'Description site boulot'),
(10, 'site-enfants', 'www.site-enfants.com', '2020-09-03', 'Description site enfants'),
(11, 'site-achat', 'www.site-achat.com', '2020-10-13', 'Description site achat'),
(43, 'site-moi', 'site-moi.com', '2021-02-07', 'blabla moi'),
(44, 'site-essaiy', 'y@.com', '2021-02-07', 'blabla y');

-- --------------------------------------------------------

--
-- Structure de la table `ressources_via_categories`
--

DROP TABLE IF EXISTS `ressources_via_categories`;
CREATE TABLE IF NOT EXISTS `ressources_via_categories` (
  `id_ressource` int(11) NOT NULL,
  `id_categories` int(11) NOT NULL,
  PRIMARY KEY (`id_ressource`,`id_categories`),
  KEY `id_categories` (`id_categories`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ressources_via_categories`
--

INSERT INTO `ressources_via_categories` (`id_ressource`, `id_categories`) VALUES
(2, 2),
(3, 2),
(4, 2);

-- --------------------------------------------------------

--
-- Structure de la table `ressources_via_tag`
--

DROP TABLE IF EXISTS `ressources_via_tag`;
CREATE TABLE IF NOT EXISTS `ressources_via_tag` (
  `id_ressource` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  PRIMARY KEY (`id_ressource`,`id_tag`),
  KEY `id_tag` (`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `ressources_via_utilisateur`
--

DROP TABLE IF EXISTS `ressources_via_utilisateur`;
CREATE TABLE IF NOT EXISTS `ressources_via_utilisateur` (
  `id_ressource` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_ressource`,`id_user`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `tag`
--

DROP TABLE IF EXISTS `tag`;
CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `tag`
--

INSERT INTO `tag` (`id`, `nom`) VALUES
(1, 'tag-heur'),
(2, 'tag-ada'),
(3, 'tag-euse'),
(4, 'tag-ada-tsoin-tsoin'),
(5, 'tag-liatelles'),
(6, 'tag-alopé'),
(7, 'tag-rossit'),
(8, 'tag-argouillé'),
(9, 'tag-agné'),
(10, 'tag-eulé'),
(11, 'tag-randit');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `adresse`) VALUES
(1, 'MARGERY', 'Marie', 'Adresse de Marie 43800'),
(2, 'PEYRON', 'Estelle', 'Adresse de Estelle 43000'),
(3, 'Zentai', 'Zsofia', 'Adresse de Zsofia 43000'),
(4, 'MARGERY', 'Christophe', 'Adresse de Christophe 43800'),
(5, 'PICARD', 'Juliette', 'Adresse de Juliette 43800'),
(6, 'MARGERY', 'Aurore', 'Adresse de Aurore 43800'),
(7, 'MARGERY', 'Florian', 'Adresse de Florian 43800'),
(8, 'PICARD', 'Dominique', 'Adresse de Dominique 43800'),
(9, 'PICARD', 'Michel', 'Adresse de Michel 43800'),
(10, 'LARGIER', 'Georges', 'Adresse de Georges 43800'),
(11, 'LARGIER', 'Claudette', 'Adresse de Claudette 43800');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `ressources_via_categories`
--
ALTER TABLE `ressources_via_categories`
  ADD CONSTRAINT `ressources_via_categories_ibfk_1` FOREIGN KEY (`id_ressource`) REFERENCES `ressource` (`id`),
  ADD CONSTRAINT `ressources_via_categories_ibfk_2` FOREIGN KEY (`id_categories`) REFERENCES `categories` (`id`);

--
-- Contraintes pour la table `ressources_via_tag`
--
ALTER TABLE `ressources_via_tag`
  ADD CONSTRAINT `ressources_via_tag_ibfk_1` FOREIGN KEY (`id_ressource`) REFERENCES `ressource` (`id`),
  ADD CONSTRAINT `ressources_via_tag_ibfk_2` FOREIGN KEY (`id_tag`) REFERENCES `tag` (`id`);

--
-- Contraintes pour la table `ressources_via_utilisateur`
--
ALTER TABLE `ressources_via_utilisateur`
  ADD CONSTRAINT `ressources_via_utilisateur_ibfk_1` FOREIGN KEY (`id_ressource`) REFERENCES `ressource` (`id`),
  ADD CONSTRAINT `ressources_via_utilisateur_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
