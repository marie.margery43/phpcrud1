<!-- Mettre à jour les enregistrements existants avec un formulaire HTML et envoyer des données au serveur avec une requête POST !-->
<?php include ('views/parts/header.php');?>

    <div class="content-update">
	<h2>Update Ressources #<?=$contact['id']?></h2>
    <form action="index.php?route=update&id=<?=$contact['id']?>" method="post">
    <p>ID</p>
        <label for="id"><input type="text" name="id" placeholder="1" value="<?=$contact['id']?>" id="id"></label>
        <p>Nom</p>
        <label for="nom"><input type="text" name="nom" placeholder="<?=$contact['nom']?>" id="nom"></label>
        <p>Lien</p>
        <label for="lien"><input type="text" name="lien" placeholder="<?=$contact['lien']?>" id="lien"></label>
        <p>Date</p>
        <label for="date"><input type="datetime-local" name="date" value="<?=date('Y-m-d\TH:i')?>" id="date"></label>
        <p>Description</p>
        <label for="description"><input type="text" name="description" placeholder="<?=$contact['description']?>" id="description"></label>
        
        <input type="submit" value="Update">
        <?php if (isset($stmt)): ?>
    <p class="sms">Ressource modifiée</p>
    <?php endif; ?>
    </form>
    
</div>

<?php include ('views/parts/footer.php');?>

