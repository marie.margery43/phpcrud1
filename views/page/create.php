<!-- Créez de nouveaux enregistrements avec un formulaire HTML et envoyez des données au serveur avec une requête POST !-->
<?php include ('views/parts/header.php');?>

    <div class="content-update">
	<h2>Creation de ressource</h2>
    <form action="index.php?route=create" method="post">
    <p>ID</p>
    <label for="id"><input type="text" name="id" placeholder="1" value="auto" id="id"></label>
        <p>Nom</p>
        <label for="nom"><input type="text" name="nom" placeholder="John Doe" id="nom"></label>
        <p>Lien</p>
        <label for="lien"><input type="text" name="lien" placeholder="johndoe@example.com" id="lien"></label>
        <p>Date</p>
        <label for="date"><input type="datetime-local" name="date" value="<?=date('Y-m-d\TH:i')?>" id="date"></label>
        <p>Description</p>
        <label for="description"><input type="text" name="description" placeholder="description" id="description"></label>
        
        
        <input type="submit" value="Creer">
        <?php if (isset($requete)): ?>
    <p class="sms">Ressource envoyée</p>
    <?php endif; ?>
    </form>
    
</div>

<?php include ('views/parts/footer.php');?>


