<?php include ('views/parts/header.php');?>

    <div class="content-read">
	<h2>Ressources / Read</h2>
	<a href="index.php?route=create" class="create-contact">Création d'un bookmark / Cliquez ici</a>
	<table>
        <thead>
            <tr>
                <td>#</td>
                <td>Nom</td>
                <td>Lien</td>
                <td>Date</td>
                <td>Description</td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contacts as $contact): ?>
            <tr>
                <td><?=$contact['id']?></td>
                <td><?=$contact['nom']?></td>
                <td><?=$contact['lien']?></td>
                <td><?=$contact['date']?></td>
                <td><?=$contact['description']?></td>
                <td class="actions">
                    <a href="index.php?route=update&id=<?=$contact['id']?>" class="edit"><i class="fas fa-pen fa-xs"></i></a>
                    <a href="index.php?route=delete&id=<?=$contact['id']?>" class="trash"><i class="fas fa-trash fa-xs"></i></a>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
	<div class="pagination">
		<?php if ($page > 1): ?>
		<a href="index.php?route=read&page=<?=$page-1?>"><i class="fas fa-angle-double-left fa-sm"></i></a>
		<?php endif; ?>
		<?php if ($page*$records_per_page < $num_contacts): ?>
		<a href="index.php?route=read&page=<?=$page+1?>"><i class="fas fa-angle-double-right fa-sm"></i></a>
		<?php endif; ?>
	</div>
</div>

<?php include ('views/parts/footer.php');?>