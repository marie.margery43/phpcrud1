<?php

function pdo_connect_mysql() {
$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'root';
$DATABASE_PASS = '';
$DATABASE_NAME = 'bookmark';
try {
    return new PDO('mysql:host=' . $DATABASE_HOST . ';dbname=' . $DATABASE_NAME . ';charset=utf8', $DATABASE_USER, $DATABASE_PASS);
} catch (PDOException $exception) {
    // If there is an error with the connection, stop the script and display the error.
    exit('Failed to connect to database!');
}
}


function get_all_bookmark($pdo, $page, $records_per_page) {
// Prepare the SQL statement and get records from our contacts table, LIMIT will determine the page
$stmt = $pdo->prepare('SELECT * FROM ressource ORDER BY id LIMIT :current_page, :record_per_page');
$stmt->bindValue(':current_page', ($page-1)*$records_per_page, PDO::PARAM_INT);
$stmt->bindValue(':record_per_page', $records_per_page, PDO::PARAM_INT);
$stmt->execute();
// Fetch the records so we can display them in our template.
$contacts = $stmt->fetchAll(PDO::FETCH_ASSOC);

return $contacts;
}

function get_count_bookmark($pdo){
// Get the total number of contacts, this is so we can determine whether there should be a next and previous button
$num_contacts = $pdo->query('SELECT COUNT(*) FROM ressource')->fetchColumn();

return $num_contacts;
}


function create_bookmark($pdo) {
    if (!empty($_POST)) {
       $id = isset($_POST['id']) && !empty($_POST['id']) && $_POST['id'] != 'auto' ? $_POST['id'] : NULL;
       $nom = isset($_POST['nom']) ? $_POST['nom'] : '';
        $lien = isset($_POST['lien']) ? $_POST['lien'] : '';
        $date = isset($_POST['date']) ? $_POST['date'] : date('Y-m-d H:i:s');
        $description = isset($_POST['description']) ? $_POST['description'] : '';
        
        $insert='INSERT INTO ressource (`nom`, `lien`, `date`, `description`) VALUES (?,?,?,?)';
            $requete = $pdo -> prepare($insert); 
            $requete-> execute([$nom,$lien, $date, $description]);
            return $requete;
    }
}


// Get the contact from the contacts table
function get_ressource_bookmark($pdo) {
    $stmt = $pdo->prepare('SELECT * FROM ressource WHERE id = ?');
    $stmt->execute([$_GET['id']]);
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
}

function update_bookmark($pdo) {

    if (!empty($_POST)) {
            // partie identique au create
        $id = isset($_POST['id']) && !empty($_POST['id']) && $_POST['id'] != 'auto' ? $_POST['id'] : NULL;
        $nom = isset($_POST['nom']) ? $_POST['nom'] : '';
        $lien = isset($_POST['lien']) ? $_POST['lien'] : '';
        $date = isset($_POST['date']) ? $_POST['date'] : date('Y-m-d H:i:s');
        $description = isset($_POST['description']) ? $_POST['description'] : '';

        $stmt = $pdo->prepare('UPDATE ressource SET id = ?, nom = ?, lien = ?, date = ?, description = ? WHERE id = ?');
        $stmt->execute([$id, $nom, $lien, $date, $description, $id]);
        header('Location: index.php?route=read&message=updateok');
        return $stmt; 
    }
}


function delete_bookmark($pdo) {
    
        if ($_GET['confirm'] == 'yes') {
            // User clicked the "Yes" button, delete record
            $stmt = $pdo->prepare('DELETE FROM ressource WHERE id = ?');
            $stmt->execute([$_GET['id']]);
        } else {
            // User clicked the "No" button, redirect them back to the read page
            header('Location: index.php?route=read&page=1');
            exit;
        }
        return $stmt;
    }
 


    
?>
